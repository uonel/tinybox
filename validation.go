package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"
)

type IndieAuthRes struct {
	Me       string `json:"me"`
	ClientId string `json:"client_id"`
	Scope    string `json:"scope"`
	Issue    int    `json:"issued_at"`
	Nonce    int    `json:"nonce"`
}

// TODO: Scrape token endpoint from url and save it. Then use saved one and only rescrape if saved one doesn't work.
func checkAccess(token string) (bool, error) {
	if token == "" {
		return false, errors.New("token string is empty")
	}
	// form the request to check the token
	client := http.DefaultClient
	req, err := http.NewRequest("GET", IndieAuthTokenUrl, nil)
	if err != nil {
		return false, errors.New("error making the request for checking token access")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
	// send the request
	res, err := client.Do(req)
	if err != nil {
		return false, errors.New("error sending the request for checking token access")
	}

	// parse the response
	indieAuthRes := &IndieAuthRes{}
	err = json.NewDecoder(res.Body).Decode(&indieAuthRes)
	res.Body.Close()
	if err != nil {
		return false, errors.New("the token endpoint didn't return valid json")
	}
	// verify results of the response
	if CleanUrl(indieAuthRes.Me) != SiteUrl {
		return false, errors.New("wrong token (me does not match)")
	}
	scopes := strings.Fields(indieAuthRes.Scope)
	scopeReached := false
	for _, scope := range scopes {
		if scope == "media" || scope == "create" {
			scopeReached = true
			break
		}
	}
	if !scopeReached {
		return false, errors.New("you only have permission to " + indieAuthRes.Scope + " not media")
	}
	return true, nil
}

func ExtractToken(r *http.Request) string {
	if token := r.Header.Get("authorization"); len(token) > 0 {
		return strings.Replace(token, "Bearer ", "", 1)
	}
	return r.FormValue("access-token")
}

func CleanUrl(url string) string {
	url = strings.Replace(url, "https://", "", 1)
	url = strings.Replace(url, "http://", "", 1)
	if strings.HasSuffix(url, "/") {
		url = url[:len(url)-1]
	}
	return url
}
