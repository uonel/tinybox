package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
)

var (
	removeCharacters = regexp.MustCompile(`[^a-zA-U\d-]`)
	fileRegex        = regexp.MustCompile(`^(.+)(\.[a-z]{2,3})$`)
)

func HandleMedia(w http.ResponseWriter, r *http.Request) (int, error) {
	// check authorization
	token := ExtractToken(r)
	authorized, tokenErr := checkAccess(token)
	switch r.Method {
	case "GET":
		filere := regexp.MustCompile(`([^\/]+)$`)
		filepath := FileDir + "/" + filere.FindString(r.URL.Path)
		log.Println("Serving " + filepath)
		filestat, err := os.Stat(filepath)
		if err != nil {
			return 404, errors.New("File not Found")
		}
		if !authorized && filestat.IsDir() {
			return 401, errors.New("Unauthorized")
		}
		http.ServeFile(w, r, filepath)
	case "POST":
		if tokenErr != nil {
			return 401, tokenErr
		}
		if authorized {
			err := r.ParseMultipartForm(32 << 20) // in- or decrease this?
			if err != nil {
				fmt.Println(err)
				return 400, err
			}
			uf, h, err := r.FormFile("file")
			filenamevalue := []byte(h.Filename)
			name := fileRegex.ReplaceAll(filenamevalue, []byte("$1"))
			extension := fileRegex.ReplaceAll(filenamevalue, []byte("$2"))
			name = removeCharacters.ReplaceAll(name, []byte(""))
			filename := string(name) + string(extension)
			counter := 0
			_, err = os.Stat(FileDir + "/" + filename)
			fileexists := err == nil
			for fileexists {
				filename = string(name) + strconv.Itoa(counter) + string(extension)
				_, err = os.Stat(FileDir + "/" + filename)
				fileexists = err == nil
				counter += 1
			}
			f, _ := os.Create(FileDir + "/" + filename)
			io.Copy(f, uf)
			log.Println("save " + filename)
			w.Header().Add("Location", ServeUrlPrefix+"/"+filename)
			w.WriteHeader(201)
		}
	default:
		return 400, errors.New("Only POST and GET requests are allowed")
	}
	return 200, nil
}
