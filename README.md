# Tinybox


```
A simple micropub media endpoint that saves files to disk and optionally
	serves them.

Usage:
  tinybox site-url [flags]

Flags:
  -f, --filedir string   The directory files will be written and served from (default "files")
  -h, --help             help for tinybox
  -p, --port string      The port to listen (default "5556")
  -t, --token string     Your token endpoint (default "https://tokens.indieauth.com/token")
  -u, --url string       Url images are served from
```

