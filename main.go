package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/spf13/cobra"
)

var (
	FileDir           = "files"
	SiteUrl           string
	ServeUrlPrefix    string
	IndieAuthTokenUrl = "https://tokens.indieauth.com/token"
	port              string
)

var rootCmd = &cobra.Command{
	Use:   "tinybox site-url",
	Short: "A micropub media endpoint",
	Long: `A simple micropub media endpoint that saves files to disk and optionally
	serves them.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		log.Println("Starting micropub media endpoint...")
		SiteUrl = CleanUrl(args[0])
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			status, err := HandleMedia(w, r)
			if err != nil {
				w.WriteHeader(status)
				w.Write([]byte(err.Error()))
			}
		})
		log.Println("Site Url: " + SiteUrl)
		log.Println("Listening on Port " + port)
		log.Println("Saving Files to " + FileDir)
		log.Fatal(http.ListenAndServe(":"+port, nil))
	},
}

func main() {
	rootCmd.Flags().StringVarP(&port, "port", "p", "5556", "The port to listen")
	rootCmd.Flags().StringVarP(&FileDir, "filedir", "f", "files", "The directory files will be written and served from")
	rootCmd.Flags().StringVarP(&ServeUrlPrefix, "url", "u", "", "Url images are served from")
	rootCmd.Flags().StringVarP(&IndieAuthTokenUrl, "token", "t", "https://tokens.indieauth.com/token", "Your token endpoint")
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
